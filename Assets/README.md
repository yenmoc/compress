# Compress

## What
	-System.IO.Compression.ZipFile.dll version 4.3.0
	-System.IO.Compression version.dll 4.3.0

## Installation

```bash
"com.yenmoc.compress":"https://gitlab.com/yenmoc/compress"
or
npm publish --registry=http://localhost:4873
```
